﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebFrontAppChallengev1.Models;
using System.Text;


namespace WebFrontAppChallengev1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // -----------URL del web API
        //-************************************
        string BaseUrl = "http://sig.ventcorp.com/WebApiAppChallenge/";

        //--Listar Todos los datos 
        public async Task<ActionResult> ListarEvaluaciones()
        {
            ActionResult actionResult = null;
            List<MEvaluaciones> lstEvaluaciones =  new List<MEvaluaciones>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Trae todos los registros de evaluaciones
                HttpResponseMessage res = await client.GetAsync("api/Evaluaciones");
                if(res.IsSuccessStatusCode){
                    var EvalResponse = res.Content.ReadAsStringAsync().Result;
                    //Deserializar los datos
                    lstEvaluaciones = JsonConvert.DeserializeObject<List<MEvaluaciones>>(EvalResponse);
                }
            }

            actionResult = Content(JsonConvert.SerializeObject(lstEvaluaciones)); 
            return actionResult;
        }

        //--Listar Todos los datos Pero una sola Evaluacion
        public async Task<ActionResult> ListarEvaluacionPorID(string sIdEvaluacion)
        {
            ActionResult actionResult = null;
            MEvaluaciones _MEvaluaciones = new MEvaluaciones();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //Trae todos los registros de evaluaciones
                string completar_url = "api/Evaluaciones/" + sIdEvaluacion;
                HttpResponseMessage res = await client.GetAsync(completar_url);
                if (res.IsSuccessStatusCode)
                {
                    var EvalResponse = res.Content.ReadAsStringAsync().Result;
                    //Deserializar los datos
                    _MEvaluaciones = JsonConvert.DeserializeObject<MEvaluaciones>(EvalResponse);                    
                }
            }

            actionResult = Content(JsonConvert.SerializeObject(_MEvaluaciones));
            return actionResult;
        }

        //--CREAR Y MODIFICAR
        [HttpPost]
        public ActionResult CrearEvaluacion(string sNombres, string sCorreo, string sCalificacion, string id, string action)
        {
            ActionResult actionResult = null;
            MEvaluaciones _MEvaluaciones = new MEvaluaciones();
            if (id == "0" && action=="Grabar")
            {
                _MEvaluaciones.lIdEvaluacionServicio = 0;
            }else{
                _MEvaluaciones.lIdEvaluacionServicio = Convert.ToInt64(id);
            }
            

            _MEvaluaciones.sNombres = sNombres;
            _MEvaluaciones.sCorreo = sCorreo;
            _MEvaluaciones.dFechaRegistro = DateTime.Now;
            _MEvaluaciones.iCalificacion = Convert.ToInt32(sCalificacion);
            MResultado _MResultado = new MResultado();
            using (var client = new HttpClient())
            {
                string url = BaseUrl + "api/Evaluaciones";
                client.BaseAddress = new Uri(url);
                
                if (id == "0" && action == "Grabar")
                {
                    var respuesta = client.PostAsJsonAsync<MEvaluaciones>("evaluaciones", _MEvaluaciones);
                    respuesta.Wait();
                    var result = respuesta.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var EvalResponse = result.Content.ReadAsStringAsync().Result;
                        //Deserializar los datos
                        _MResultado = JsonConvert.DeserializeObject<MResultado>(EvalResponse);
                    }
                }
                else
                {
                    var respuesta = client.PutAsJsonAsync<MEvaluaciones>("evaluaciones", _MEvaluaciones);
                    respuesta.Wait();
                    var result = respuesta.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var EvalResponse = result.Content.ReadAsStringAsync().Result;
                        //Deserializar los datos
                        _MResultado = JsonConvert.DeserializeObject<MResultado>(EvalResponse);
                    }
                }

                
            }

            actionResult = Content(JsonConvert.SerializeObject(_MResultado));
            return actionResult;
        }


        //--ELIMINACION
        [HttpPost]
        public ActionResult EliminarEvaluacion(string id)
        {
            ActionResult actionResult = null;            
            MResultado _MResultado = new MResultado();
            using (var client = new HttpClient())
            {
                string url = BaseUrl + "api/Evaluaciones/"+id;
                client.BaseAddress = new Uri(url);
                var resDele= client.DeleteAsync(url);
                resDele.Wait();
                var result = resDele.Result;
                if (result.IsSuccessStatusCode)
                {
                    var EvalResponse = result.Content.ReadAsStringAsync().Result;
                    //Deserializar los datos
                    _MResultado = JsonConvert.DeserializeObject<MResultado>(EvalResponse);
                }
            }

            actionResult = Content(JsonConvert.SerializeObject(_MResultado));
            return actionResult;
        }


        //-----CONSUMO SERVICIO SOAP

        public async Task<ActionResult> BuscarEvaluacionesSOAP(string sFechaIni, string sFechaFin)
        {
            ActionResult actionResult = null;
            DateTime dFechaInicio = Convert.ToDateTime(sFechaIni);
            DateTime dFechaFin = Convert.ToDateTime(sFechaFin);

            List<MEvaluaciones> lstEvaluaciones = new List<MEvaluaciones>();
            MEvaluaciones _MEvaluaciones = new MEvaluaciones();
            using( ServiceReferenceEvaluacionesSOAP.Service1Client objServiceClient = new ServiceReferenceEvaluacionesSOAP.Service1Client()){
                var evaluaciones = objServiceClient.GetListarEvaluaciones(dFechaInicio,dFechaFin);
                foreach (var datos in evaluaciones)
                {
                    _MEvaluaciones = new MEvaluaciones();
                    _MEvaluaciones.lIdEvaluacionServicio = Convert.ToInt64(datos.lIdEvaluacionServicio);
                    _MEvaluaciones.sNombres = datos.sNombres;
                    _MEvaluaciones.sCorreo = datos.sCorreo;
                    _MEvaluaciones.iCalificacion = Convert.ToInt32(datos.iCalificacion);
                    _MEvaluaciones.dFechaRegistro = Convert.ToDateTime(datos.dFechaRegistro);
                    lstEvaluaciones.Add(_MEvaluaciones);
                }
            };

            actionResult = Content(JsonConvert.SerializeObject(lstEvaluaciones));
            return actionResult;
        }






    }
}