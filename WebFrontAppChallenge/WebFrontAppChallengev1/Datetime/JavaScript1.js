﻿
$(function () {
    $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $.fn.datepicker.defaults.language = "es";
    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.todayHighlight = true;

    $("#datetimeIni").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true//,
        //startDate: dateTime
    });

    $("#datetimeFin").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $("#datetimeIni").datepicker().datepicker("setDate", new Date());
    $("#datetimeFin").datepicker().datepicker("setDate", new Date());

});

