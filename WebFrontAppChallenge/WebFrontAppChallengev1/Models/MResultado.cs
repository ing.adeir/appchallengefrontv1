﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFrontAppChallengev1.Models
{
    public class MResultado
    {
        public Boolean bResultado { get; set; }
        public string sMensaje { get; set; }
    }
}