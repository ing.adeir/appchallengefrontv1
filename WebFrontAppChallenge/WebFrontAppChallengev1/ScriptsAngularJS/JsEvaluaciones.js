﻿
var app = angular.module('crudApp', []);
app.controller('crudController', function ($scope,$timeout, $http) {

    $timeout(function () {
        //-----Load
        // $scope.ListadoDatos();
    });

    $scope.success = false;
    $scope.error = false;

    $scope.ListadoDatos = function () {
        $http.get('/Home/ListarEvaluaciones').success(function (data) {
            $scope.namesData = data;
        });
    };

    $scope.openModal = function () {
        var modal_popup = angular.element('#crudmodal');
        modal_popup.modal('show');
    };

    $scope.closeModal = function () {
        var modal_popup = angular.element('#crudmodal');
        modal_popup.modal('hide');
    };

    $scope.addData = function () {
        $scope.txtNombres = "";
        $scope.txtCorreo = "";
        $scope.txtCalificacion = "0";
        $scope.hidden_id = "0";
        $scope.modalTitle = 'Nueva Evaluacion';
        $scope.submit_button = 'Grabar';
        $scope.openModal();
    };

    $scope.submitForm = function () {
        $http({
            method: "POST",
            url: "/Home/CrearEvaluacion",
            data: { 'sNombres': $scope.txtNombres, 'sCorreo': $scope.txtCorreo, 'sCalificacion': $scope.txtCalificacion, 'id': $scope.hidden_id, 'action': $scope.submit_button }
        }).success(function (data) {
            if (data.bResultado != true) {
                $scope.success = false;
                $scope.error = true;
                $scope.errorMessage = data.sMensaje;
            }
            else {
                $scope.success = true;
                $scope.error = false;
                $scope.successMessage = data.sMensaje;
                $scope.form_data = {};
                $scope.closeModal();
                $scope.ListadoDatos();
            }
        });
    };

    $scope.ListarUnaEvaluacion = function (id) {
        $http({
            method: "POST",
            url: "/Home/ListarEvaluacionPorID",
            data: { 'sIdEvaluacion': id }
        }).success(function (data) {
            $scope.txtNombres = data.sNombres;
            $scope.txtCorreo = data.sCorreo; 
            $scope.txtCalificacion = data.iCalificacion;
            $scope.hidden_id = id;
            $scope.modalTitle = 'Modificar Evaluacion';
            $scope.submit_button = 'Editar';
            $scope.openModal();
        });
    };

    $scope.deleteData = function (id) {
        if (confirm("¿Estas seguro que quieres eliminar esta Evaluacón?")) {
            $http({
                method: "POST",
                url: "/Home/EliminarEvaluacion",
                data: { 'id': id }
            }).success(function (data) {
                $scope.success = true;
                $scope.error = false;
                $scope.successMessage = data.sMensaje;
                $scope.ListadoDatos();
            });
        }
    };

    //--Busqueda
    $scope.BuscarPorFecha = function () {
        $http({
            method: "POST",
            url: "/Home/BuscarEvaluacionesSOAP",
            data: { 'sFechaIni': $scope.datetimeIni, 'sFechaFin': $scope.datetimeFin }
        }).success(function (data) {
            $scope.namesData = data;
        });
    };


});

