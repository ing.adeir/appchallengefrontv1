﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebFrontAppChallengev1.Startup))]
namespace WebFrontAppChallengev1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
